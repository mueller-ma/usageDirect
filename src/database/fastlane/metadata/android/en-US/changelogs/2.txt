Visual improvements (tabs, less tight content, larger icons) while maintaining high performance
Fix bug with hour values "modulo 60"
Feature preview: charts
